@extends('management.dashboard.layout')
@section('body')
    <style>
        #triangle-left{
            width: 0;
            height: 0;
            border-top: 50px solid transparent;
            border-right: 100px solid red;
            border-bottom: 50px solid transparent;
        }
    </style>
    <div ng-controller="homeCtr" ng-init="initializeMe();renderChartsPie();setFilter(ones[0]);viewFn();clickFn();playFn();" style="width: 98%">
        <main class="col-sm-12 offset-sm-3 col-md-12 offset-md-2 pt-3" style="border-radius: 0.25rem;padding: 45px;margin-bottom: 8px;">
            <form novalidate name="formSearchStats" id="formSearchStats">
                <div class="form-group">
                    <div class="col-xs-6" >
                        <h4 style="float: left;">Overviews</h4>
                    </div>
                    {{--<div class="col-xs-6" style="padding: 0px;">
                        <div class="demo" style="float: right;width: 40%;">
                            <input date-range-picker id="daterange3" name="daterange3" class="form-control date-picker" type="text"
                                   ng-model="daterangepckr" options="opts" required/>
                        </div>
                    </div>--}}
                </div>
            </form>
            <br>
            <br>
            <hr>
            <div class="table-responsive" style="overflow-x: hidden;">
                <div class="card" style="border: 0px;">
                    <div class="row">
                        <div class="col-6 card" style="border-bottom: 0px;">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="card-block" style="padding: 0px;">
                                        <span class="card-link">Subscribed</span>
                                        <span class="card-link" style="float: right;" ng-cloak="">@{{ userSubscribed }} / @{{ total_sub }}</span>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="card-block" style="padding: 0px;">
                                        <span class="card-link">Unsubscribed</span>
                                        <span class="card-link" style="float: right;" ng-cloak="">@{{ userUnsubscribed }} / @{{ total_sub }}</span>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="card-block" style="padding: 0px;">
                                        <span class="card-link">Platinum</span>
                                        <span class="card-link" style="float: right;" ng-cloak="">@{{ liststatsPie[2].current_sub }}</span>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="card-block" style="padding: 0px;">
                                        <span class="card-link">Gold</span>
                                        <span class="card-link" style="float: right;" ng-cloak="">@{{ liststatsPie[1].current_sub }}</span>
                                    </div>
                                </li>
                                <li class="list-group-item" style="border-bottom: 1px solid #e0e0e0;">
                                    <div class="card-block" style="padding: 0px;">
                                        <span class="card-link">Silver</span>
                                        <span class="card-link" style="float: right;" ng-cloak="">@{{ liststatsPie[0].current_sub }}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-6 card">
                            <div class="card-block clearfix" id="containerPie" style="height: 454px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <br>
        <main class="col-sm-12 offset-sm-3 col-md-12 offset-md-2 pt-3" style="padding: 45px;margin-bottom: 8px;border-radius: 0.25rem;">
            <div class="table-responsive" style="overflow-x: hidden;">
                <div class="row">
                    <div class="col-6">
                        {{--<div class="row" style="margin-top: 10px;">
                            <div class="col-3" style="padding: 0px;margin-left: 22px;">
                                <select name="type" class="form-control" id="one" ng-model="filter.one" ng-options="one as one.name for one in ones" style="width: 100%;font-size: 12px;border-radius: 8px;">
                                </select>
                            </div>
                        </div>--}}
                        <div class="clearfix" style="float:left;margin-top: 10px;">
                            <div id="selectdatehometype" class="btn-group btn-group-type" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-secondary btn-sm btnaktif" style="color: black;font-family: calibri;font-weight: 600;" ng-click="setPage('all')">All</button>
                                <button type="button" class="btn btn-secondary btn-sm" style="color: black;font-family: calibri;font-weight: 600;" ng-click="setPage('Mobile')">Mobile</button>
                                <button type="button" class="btn btn-secondary btn-sm" style="color: black;font-family: calibri;font-weight: 600;" ng-click="setPage('TV')">TV</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        {{--<div class="row" style="margin-top: 10px;">
                            <div class="col-3" style="padding: 0px;margin-left: 22px;">
                                <select name="type" class="form-control" id="one" ng-model="filter.one" ng-options="one as one.name for one in ones" style="width: 100%;font-size: 12px;border-radius: 8px;">
                                </select>
                            </div>
                        </div>--}}
                        <div class="clearfix" style="float:left;margin-top: 10px;">
                            <div id="selectdatehome" class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-secondary btn-sm" style="color: black;font-family: calibri;font-weight: 600;" ng-click="setTipeFn('hourly')">Hourly</button>
                                <button type="button" class="btn btn-secondary btn-sm btnaktif" style="color: black;font-family: calibri;font-weight: 600;" ng-click="setTipeFn('daily')">Day</button>
                                <button type="button" class="btn btn-secondary btn-sm" style="color: black;font-family: calibri;font-weight: 600;" ng-click="setTipeFn('week')">Week</button>
                                <button type="button" class="btn btn-secondary btn-sm" style="color: black;font-family: calibri;font-weight: 600;" ng-click="setTipeFn('month')">Month</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div id="groupiconcalendar" class="demo clearfix" style="">
                            <input date-range-picker id="daterange3" name="daterange3" class="form-control date-picker" type="text"
                                   ng-model="daterangepckr" options="opts" required/>
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <br>
                        <br>
                        <div>
                            <ul id="selecttipeX" class="list-group">
                                    <li id="open" class="list-custom list-custom-active list-group-item remove-border-left remove-border-right" ng-click="setFilter(ones[0])" style="padding: 5px;">
                                        <span class="col-12">Open Edukids</span>
                                            <br>
                                        <span class="col-12 highlight-list" ng-cloak="">@{{ listMerge._open }}</span>
                                    </li>
                                    <li id="crash" class="list-custom list-group-item remove-border-left remove-border-right" ng-click="setFilter(ones[1])" style="padding: 5px;">
                                        <span class="col-12">Application Crash</span>
                                            <br>
                                        <span class="col-12 highlight-list" ng-cloak="">@{{ listMerge._crash }}</span>
                                    </li>
                                    <li id="subscribe" class="list-custom list-group-item remove-border-left remove-border-right" ng-click="setFilter(ones[2])" style="padding: 5px;">
                                        <span class="col-12">Subscribe</span>
                                            <br>
                                        <span class="col-12 highlight-list" ng-cloak="">@{{ listMerge._subscribe }}</span>
                                    </li>
                                    <li id="unsubscribe" class="list-custom list-group-item remove-border-left remove-border-right" ng-click="setFilter(ones[3])" style="padding: 5px;">
                                        <span class="col-12">Unsubscribe</span>
                                            <br>
                                        <span class="col-12 highlight-list" ng-cloak="">@{{ listMerge._unsubscribe }}</span>
                                    </li>
                            </ul>
                        </div>
                        <br>
                        <br>
                    </div>
                    <div class="col-8" style="border-left: 1px solid #c4c4c4;">
                        <br>
                        <div id="container" style="height: 454px;" class="clearfix"></div>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </main>
        <br>
        <div id="mostData" class="row" ng-cloak="" style="margin-top: -8px;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 7px;">
                <div class="card custom-merge" style="padding: 20px;box-shadow: 1px 1px 1px 1px grey;">
                    <div class="table-responsive" style="overflow-x: hidden;">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <h5>Most Clicked</h5>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <select name="utama_id" class="form-control" ng-model="clickTable.utama_id">
                                        <option value="">All</option>
                                        <option value="2">Animation</option>
                                        <option value="3">EBook</option>
                                        <option value="8">Music</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="padding: 0px;">
                            <div class="col-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Title</th>
                                        <th>Hit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="(i,x) in list_click" ng-cloak="">
                                        <td>@{{ i+1 }}.</td>
                                        <td style="white-space: nowrap;text-overflow: ellipsis;max-width: 220px;overflow: hidden;">@{{ x.nama }}</td>
                                        <td>@{{ x._click }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 7px;padding-left: 15px;">
                <div class="card custom-merge" style="padding: 20px;box-shadow: 1px 1px 1px 1px grey;">
                    <div class="table-responsive" style="overflow-x: hidden;">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <h5>Most Viewed</h5>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <select name="utama_id" class="form-control" ng-model="viewTable.utama_id">
                                        <option value="">All</option>
                                        <option value="2">Animation</option>
                                        <option value="3">EBook</option>
                                        {{--<option value="8">Music</option>--}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="padding: 0px;">
                            <div class="col-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Title</th>
                                        <th>Hit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="(i,x) in list_view" ng-cloak="">
                                        <td>@{{ i+1 }}.</td>
                                        <td style="white-space: nowrap;text-overflow: ellipsis;max-width: 220px;overflow: hidden;">@{{ x.nama }}</td>
                                        <td>@{{ x._view }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 7px;padding-right: 15px;">
                <div class="card custom-merge" style="padding: 20px;box-shadow: 1px 1px 1px 1px grey;">
                    <div class="table-responsive" style="overflow-x: hidden;">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <h5>Most Played</h5>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <select name="utama_id" class="form-control" ng-model="playTable.utama_id">
                                        <option value="">All</option>
                                        <option value="2">Animation</option>
                                        <option value="3">EBook</option>
                                        <option value="8">Music</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="padding: 0px;">
                            <div class="col-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Title</th>
                                        <th>Hit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="(i,x) in list_play" ng-cloak="">
                                        <td>@{{ i+1 }}.</td>
                                        <td style="white-space: nowrap;text-overflow: ellipsis;max-width: 220px;overflow: hidden;">@{{ x.nama }}</td>
                                        <td>@{{ x._play }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <main class="col-sm-12 offset-sm-3 col-md-12 offset-md-2 pt-3" style="height: auto;border-radius: 0.25rem;padding: 45px;margin-bottom: 8px;">
            <form novalidate name="formSearchStats" id="formSearchStats">
                <div class="form-group">
                    <div class="col-xs-6" >
                        <h4 style="float: left;">Crash History</h4>
                    </div>
                </div>
            </form>
            <br>
            <hr>
            <div class="row">
                <div class="col-4">
                    <div class="clearfix" style="width:60%;float:left;margin-top: 10px;">
                        <select name="package_name" class="form-control" ng-model="report3.package_name">
                            <option value="id.edukids.apps">Mobile</option>
                            <option value="id.edukids.apps.tv">TV</option>
                            {{--<option value="8">Music</option>--}}
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <div class="clearfix" style="width:60%;float:left;margin-top: 10px;">
                        <select name="type" class="form-control" ng-model="report3.type">
                            <option value="800">Critical</option>
                            <option value="801">Verbose</option>
                            <option value="802">Debug</option>
                            <option value="803">Info</option>
                            <option value="804">Warning</option>
                            <option value="805">Error</option>
                            <option value="806">Assert</option>
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <div id="groupiconcalendar" class="demo clearfix" style="">
                        <input date-range-picker id="daterange3" name="daterange3" class="form-control date-picker" type="text"
                               ng-model="daterangepckrbug" options="optsbug" required/>
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                    </div>
                </div>
            </div>
            <br>
            <div class="table-responsive" style="height: auto;overflow-y: auto;overflow-x: hidden;">
                <div class="card" style="border: 0px;">
                    <div class="row">
                        <div class="col-12 card">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal</th>
                                    <th>Device</th>
                                    <th>OS</th>
                                    <th>Version</th>
                                    <th>Type</th>
                                    <th>Subject</th>
                                    <th>Message</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="(i,x) in listCrash" ng-cloak="">
                                    <td>@{{ startNumber+i }}</td>
                                    <td>@{{ x.tanggal | amUtc | amDateFormat:'dddd, Do MMMM YYYY' }}</td>
                                    <td>@{{ x.device_type }}(@{{ x.device_brand }})</td>
                                    <td>@{{ x.device_os }}</td>
                                    <td>@{{ x.device_version }}</td>
                                    <td>@{{ x.type }}</td>
                                    <td>@{{ x.subject }}</td>
                                    <td>@{{ x.message }}</td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="center_pagination" ng-cloak="">
                                <div class="list_pagination" style="cursor: pointer;">
                                    <a ng-class="{'graybg cursornodrop' : current_page === 1}" ng-if="current_page === 1" ng-cloak="" >&laquo;</a>
                                    <a ng-class="{'graybg cursornodrop' : current_page === 1}" ng-if="current_page != 1" ng-click="pagingFunction(current_page-1,current_page);" ng-cloak="" >&laquo;</a>
                                    <a ng-repeat="ii in [] | newpaging:current_page:last_page" ng-class="{'active cursornodrop' : current_page === ii}" ng-click="pagingFunction(ii,current_page);">
                                        @{{ ii }}
                                    </a>
                                    <a ng-class="{'graybg cursornodrop' : current_page === total_page}" ng-if="current_page === total_page" ng-cloak="" >&raquo;</a>
                                    <a ng-class="{'graybg cursornodrop' : current_page === total_page}" ng-if="current_page != total_page" ng-click="pagingFunction(current_page+1,current_page);" ng-cloak="" >&raquo;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection