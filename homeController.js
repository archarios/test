app.controller('homeCtr',['$rootScope','$scope','$http','MyService', function ($rootScope,$scope,$http,$svc){
    $rootScope.$emit('onPage',1);
    $scope.list = [];
    $scope.list_view = [];
    $scope.list_click = [];
    $scope.list_play = [];
    $scope.data = {};
    $scope.detail = {};
    $scope.src = {};
    $scope.report = {};
    $scope.report2 = {};
    $scope.report3 = {
        package_name : 'id.edukids.apps',
        type : '800'
    };
    $scope.filter = {};
    $scope.reporttime = 'daily';
    $scope.reporttime = 'daily';
    $scope.dataUrl = $svc.dataUrl;

    $scope.report.name = [];
    $scope.months = $svc.options.months;
    $scope.years = $svc.options.years;
    $scope.report = {
        month : $scope.months[0].id,
    };
    var bd=0;
    var cd=0;
    var hsl = 0;

    //Auth key
    $scope.testlibstats ='';

    var statisticlib = new Statistic({
        auth : "group",
        app_id : "EDUKIDS",
        app_secret : "7371de1d1c408fd0da4bd546aa1d39a68f2cd021",
        app_public : "5621bb082f168389cd635b433997e2abf726c5ae"
    });
    //Auth key
    var authHeader = {
        'X-OWTC-Type':'group',
        'X-OWTC-App-Id':statisticlib.getAppID(),
        'X-OWTC-App-Public':statisticlib.getAppPublic(),
        'X-OWTC-Token':statisticlib.getToken(),
        'X-OWTC-Id':statisticlib.getOWTCID(),
        'X-OWTC-Key':statisticlib.getOWTCKey(),
        'X-Session-Id' : angular.element('sessid').text()
    };

    $scope.statsistics = function(dtid){
        $('#container').highcharts().reflow();
    };

    $scope.newAxis = {
        min: 0,
        title: {
            text: '',
            style: {
                color: ''
            }
        }
    };
    $scope.newSeries =  {
        name: 'name',
        data: [],
        yAxis:1

    };
    /*$scope.src._date = moment().subtract(1,'d').format('YYYY-MM-DD');*/

    /*==generate statistic by filter==*/
    $scope.ones = [
        {
            act : 'open',
            val : '_open',
            name : 'Open Edukids',
            type : 'action',
            category : 'application'
        },{
            act : 'crash',
            val : '_crash',
            name : 'Application Crash',
            type : 'action',
            category : 'application'
        },{
            act : 'subscribe',
            val : '_subscribe',
            name : 'Subscribe',
            type : 'transaction',
            category : 'package'
        },{
            act : 'unsubscribe',
            val : '_unsubscribe',
            name : 'Unsubscribe',
            type : 'transaction',
            category : 'package'
        }
    ];
    $scope.twoDefault = [
        {
            val : 'unique_session',
            name : 'Session'
        },
        {
            val : 'unique_device',
            name : 'Device'
        },
        {
            val : 'unique_visitor',
            name : 'Visitor'
        }
    ];
    $scope.twos = [];



    $scope.initializeMe = function () {
        $scope.filter.one = $scope.ones[0];
    };

    $scope.setPage = function (tipe) {
        $scope.report.page_type = tipe;
        if(tipe === 'all'){
            delete $scope.report.page_type;
        }else if(tipe === 'TV'){
            $scope.report.page_type = ['TV','server'];
        }

        $scope.renderCharts();
    };

    $scope.setTipeFn = function (tipe) {
        $scope.reporttime = tipe;
        $scope.renderCharts();
    };

    $scope.setFilter = function (newData) {

        /*$('li.list-custom-active').removeClass('current');
        $(this).closest('li').addClass('current');*/

        $scope.filter.one = newData;
        /*console.log(newData);*/
        $scope.report.category = newData.category;
        $scope.report.type = newData.type;
        $scope.report.name = [];
        $scope.report.name.push(newData.act);
        if($scope.option.yAxis.length > 1){
            $scope.option.yAxis.pop();
            $scope.option.series.pop();
            delete $scope.filter.two;
        }
        $scope.option.yAxis[0].title.text = newData.name;
        $scope.twos = [];
        angular.forEach($scope.ones,function(d){
            if(d.val!==newData.val)this.push(d);
        },$scope.twos);
        $scope.twos=$scope.twos.concat($scope.twoDefault);
        $scope.renderCharts();
    };
    /*$scope.report.name.push($scope.ones[0].act);*/

    $scope.$watch('filter.one', function(newData,oldData) {
        /*console.log(newData);*/
        if(newData !== undefined){
            $scope.report.category = newData.category;
            $scope.report.type = newData.type;
            $scope.report.name = [];
            $scope.report.name.push(newData.act);
            if($scope.option.yAxis.length > 1){
                $scope.option.yAxis.pop();
                $scope.option.series.pop();
                delete $scope.filter.two;
            }
            $scope.option.yAxis[0].title.text = newData.name;
            $scope.twos = [];
            angular.forEach($scope.ones,function(d){
                if(d.val!==newData.val)this.push(d);
            },$scope.twos);
            $scope.twos=$scope.twos.concat($scope.twoDefault);
            $scope.renderCharts();
        }
    }, false);

    $scope.$watch('filter.two', function(newData,oldData) {
        if(newData !== undefined && newData !== null){

            if($scope.twoDefault.indexOf(newData) === -1){
                $scope.report.name.push(newData.act);
            }else if($scope.report.name.length > 1){
                $scope.report.name = [$scope.report.name[0]];
            }
            if($scope.option.yAxis.length > 1){
                $scope.option.yAxis.pop();
                $scope.option.series.pop();
            }

            $scope.option.yAxis.push($scope.newAxis);
            $scope.option.series.push($scope.newSeries);
            $scope.newAxis.title.text = newData.name;
            $scope.newAxis.title.style.color = '#2D7773';
            $scope.newSeries.yAxis = 1;
            $scope.newSeries.color = '#2D7773';
            $scope.renderCharts();
        }else if($scope.option.yAxis.length > 1){
            $scope.option.yAxis.pop();
            $scope.option.series.pop();
            $scope.renderCharts();
        }
    });
    /*==generate statistic by filter==*/

    /*==generate statistic by date==*/
    /*===datepicker==*/
    $scope.daterangepckr = {
        startDate: moment().subtract(7, "days"),
        endDate: moment().subtract(1, "days")
    };

    $scope.opts = {
        locale: {
            applyClass: 'btn-green',
            applyLabel: "Apply",
            fromLabel: "From",
            format: "YYYY-MM-DD",
            toLabel: "To",
            cancelLabel: 'Cancel',
            customRangeLabel: 'Custom range'
        },
        ranges: {
            'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1,'days')],
            'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1,'days')]
        },
        opens:'left'
    };

    //Watch for date changes
    $scope.$watch('daterangepckr', function(newDate) {
        if(newDate !== undefined){
            $scope.report.start_date = newDate.startDate.format('YYYY-MM-DD');
            $scope.report.end_date= newDate.endDate.format('YYYY-MM-DD');
            $scope.renderCharts();
        }
    }, false);

    /*===datepicker==*/

    /*===datepicker FOR BUG==*/
    $scope.daterangepckrbug = {
        startDate: moment().subtract(30, "days"),
        endDate: moment().subtract(1, "days")
    };

    $scope.optsbug = {
        locale: {
            applyClass: 'btn-green',
            applyLabel: "Apply",
            fromLabel: "From",
            format: "YYYY-MM-DD",
            toLabel: "To",
            cancelLabel: 'Cancel',
            customRangeLabel: 'Custom range'
        },
        ranges: {
            'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1,'days')],
            'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1,'days')]
        },
        opens:'left'
    };

    //Watch for date changes
    $scope.$watch('daterangepckrbug', function(newDate) {
        if(newDate !== undefined){
            $scope.report3.start = newDate.startDate.format('YYYY-MM-DD');
            $scope.report3.end = newDate.endDate.format('YYYY-MM-DD');
            $scope.mergeDataCrash();
        }
    }, false);

    //Watch for date changes
    $scope.$watch('report3.package_name', function(ndata) {
        if(ndata !== undefined){
            $scope.report3.package_name = ndata;
            $scope.mergeDataCrash();
        }
    }, false);

    //Watch for date changes
    $scope.$watch('report3.type', function(ndata) {
        if(ndata !== undefined){
            $scope.report3.type = ndata;
            $scope.mergeDataCrash();
        }
    }, false);

    /*===datepicker FOR BUG==*/
    /*==generate statistic by date==*/

//statistics
    $scope.option = {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Content Statistics'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            allowDecimals: false,
            categories: [],
            crosshair: true,
            labels : {
                enabled : false
            }
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'values',
                style: {
                    color: 'red'
                }
            }
        }],
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><br>',
            pointFormat: '<span style="color:{series.color};padding:0">{series.name} : <b>{point.y:,.0f}</b></span>'
        },
        plotOptions: {
            line: {
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'name',
            data: [],
            yAxis:0,
            color:'red'
        }]
    };

    $scope.optionPie ={
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Product Subscription'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.f}'
                },
                showInLegend: true
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}<br/>'
        },
        series: [{
            name: 'Current Subscription',
            colorByPoint: true,
            data: []
        }],
        drilldown: {
            series: []
        }
    };

    /*====================Data Render====================*/
    $scope.renderCharts = function () {
        /*console.log('masuk');*/

        $scope.report.start_date = $scope.daterangepckr.startDate.format('YYYY-MM-DD');
        $scope.report.end_date = $scope.daterangepckr.endDate.format('YYYY-MM-DD');

        $scope.report2 = angular.copy($scope.report);
        $scope.report2.merge = 'true';
        delete $scope.report2.type;
        delete $scope.report2.category;
        /*$scope.report3 = angular.copy($scope.report);
        delete $scope.report3.start_date;
        delete $scope.report3.end_date;
        $scope.report3.merge = 'true';
        $scope.report3.type = 'action';
        $scope.report3.category = 'application';*/

        /*=======================content=======================*/
        /*$scope.option.subtitle.text = moment($scope.report.end_date).format('MMMM YYYY');*/
        $http({
            url : $svc.statsServer+'statistic/event/'+$scope.reporttime,
            method : 'POST',
            headers : authHeader,
            data : $scope.report
        }).then(function (resst) {

            $scope.option.series[0].name = '';
            if($scope.option.series[1] !== undefined){
                $scope.option.series[1].name = '';
            }
            $scope.option.xAxis.categories = [];
            $scope.option.series[0].data = [];
            if($scope.option.series[1] !== undefined){
                $scope.option.series[1].data = [];
            }

            if(resst.status===200){
                if(resst.data.status===1){
                    $scope.liststats = resst.data.data;
                    $scope.option.series[0].name = $scope.filter.one.name;
                    if($scope.option.series[1] !== undefined){
                        $scope.option.series[1].name = $scope.filter.two.name;
                    }

                    $($scope.liststats).each(function (i,d) {
                        if($scope.reporttime === 'week'){
                            if(d._week === 53){
                                $scope.option.xAxis.categories.push('Week '+d._week+', '+moment().year(d._year).dayOfYear(Number(d._week * 7)).subtract(6,'days').format('DD MMM YYYY')+' - '+moment().year(d._year).month(11).date(31).format('DD MMM YYYY'));
                            }else{
                                $scope.option.xAxis.categories.push('Week '+d._week+', '+moment().year(d._year).dayOfYear(Number(d._week * 7)).subtract(6,'days').format('DD MMM YYYY')+' - '+moment().year(d._year).dayOfYear(Number(d._week*7)).format('DD MMM YYYY'));
                            }
                        }else if($scope.reporttime === 'hourly'){
                            $scope.option.xAxis.categories.push(moment(d._date).hour(d._hour).format('dddd, DD MMMM YYYY, HH:00'));
                        }else if($scope.reporttime === 'daily'){
                            $scope.option.xAxis.categories.push(moment(d._date).format('dddd, DD MMMM YYYY'));
                        }else if($scope.reporttime === 'month'){
                            $scope.option.xAxis.categories.push(d._name+' '+d._year);
                        }

                        cd = $scope.filter.one.val;
                        var h=cd-bd;
                        hsl=(h/bd)*100;
                        $scope.option.series[0].data.push(Number(d[$scope.filter.one.val]));
                        bd=cd;

                        if($scope.option.series[1] !== undefined){
                            $scope.option.series[1].data.push(Number(d[$scope.filter.two.val]));
                        }
                    });
                } else {
                    /*console.log("gagal");*/
                    alert(resst.data.status_code);
                }
                Highcharts.chart('container', $scope.option);
            }
        });
        $scope.mergeData();
        $scope.mergeDataCrash();

        /*=======================/Data Diagram=======================*/
    };

    $scope.renderChartsPie = function () {
        /*console.log('masuk');*/

        $scope.report.start_date = $scope.daterangepckr.startDate.format('YYYY-MM-DD');
        $scope.report.end_date = $scope.daterangepckr.endDate.format('YYYY-MM-DD');

        /*=======================PIE Diagram=======================*/
        $http({
            url : $svc.apiServer+'subscription/produk',
            method : 'POST',
            headers : authHeader,
            data : {}
        }).then(function (resst) {
            if(resst.status===200){
                if(resst.data.status===1){

                    $scope.optionPie.series[0].data = [];
                    $scope.optionPie.drilldown.series = [];
                    $scope.liststatsPie = '';

                    $scope.liststatsPie = resst.data.data;
                    $($scope.liststatsPie).each(function (i,d) {
                        /*console.log(d);*/
                        var temp = {};
                        temp.name = d.name;
                        temp.y = Number(d.current_sub);
                        temp.drilldown = d.name;
                        $scope.optionPie.series[0].data.push(temp);

                        /*console.log($scope.liststatsPie);*/
                        var temp2 = {};
                        temp2.name = d.name;
                        temp2.id = d.name;
                        temp2.data = [];

                        $(d.produk).each(function (j,ee) {
                            temp2.data.push([ ee.nama, Number(ee.current_sub) ]);
                        });
                        $scope.optionPie.drilldown.series.push(temp2);
                        /*console.log($scope.optionPie);*/
                    });
                } else {
                    if(resst.data.status_code === 'ISNF'){
                        alert('Mohon maaf, Session anda telah habis');
                        window.location.href = $svc.baseURL+'log/out';
                    }else{
                        alert(resst.status + " - Err : " +resst.data.status_code);
                    }
                }
                Highcharts.chart('containerPie', $scope.optionPie);
            }
        });
        /*=======================/PIE Diagram=======================*/
    };

    $http({
        url : $svc.apiServer+'subscription/user',
        method : 'POST',
        headers : authHeader,
        data : {}
    }).then(function (resst) {
        if(resst.status===200){
            if(resst.data.status===1){
                $scope.userdata = resst.data.data;
                $scope.userSubscribed = Number($scope.userdata.current_sub);
                $scope.total_sub= Number($scope.userdata.total_sub);
                $scope.userUnsubscribed = Number($scope.userdata.total_sub - $scope.userdata.current_sub);
                /*console.log(($scope.userdata.total_sub - $scope.userdata.current_sub)/$scope.userdata.total_sub);*/
            } else {
                if(resst.data.status_code === 'ISNF'){
                    alert('Mohon maaf, Session anda telah habis');
                    window.location.href = $svc.baseURL+'log/out';
                }else{
                    alert(resst.status + " - Err : " +resst.data.status_code);
                }
            }
        }
    });

    $scope.mergeData = function () {
        $scope.report2.name = ['open','crash','subscribe','unsubscribe'];
        $http({
            url : $svc.statsServer+'statistic/event/'+$scope.reporttime,
            method : 'POST',
            headers : authHeader,
            data : $scope.report2
        }).then(function (resst) {
            if(resst.status===200){
                if(resst.data.status===1){
                    $scope.listMerge = resst.data.data;
                    /*console.log('sukses2');*/
                } else {
                    /*console.log("gagal2");*/
                    /*console.log(resst.data.status_code);*/
                }
            }
        });
    };


    /*paging*/
        $scope.total_data="";
        $scope.current_page="";
        $scope.last_page="";
        $scope.next_page_url="";
        $scope.prev_page_url="";
        $scope.per_page="";
        $scope.startNumber="";
        $scope.endNumber="";
        $scope.total_page="";
        $scope.startRange=1;
        $scope.showRange="";

        $scope.pagingFunction = function(idpage,current){
            $scope.filter.page = idpage;
            if(idpage != current){

                $http({
                    url : $svc.bugServer+'report',
                    method : 'get',
                    paramSerializer : '$httpParamSerializerJQLike',
                    params : $scope.report3
                }).then(function (res) {
                    if(res.status===200){
                        if(res.data.status===1){
                            /*paging*/
                            $scope.total_data=res.data.data.total;
                            $scope.current_page=res.data.data.current_page;
                            $scope.last_page=res.data.data.last_page;
                            $scope.total_page=$scope.last_page;
                            $scope.next_page_url=res.data.data.next_page_url;
                            $scope.prev_page_url=res.data.data.prev_page_url;
                            $scope.startNumber=res.data.data.from;
                            $scope.endNumber=res.data.data.to;
                            $scope.per_page=res.data.data.per_page;
                            /*paging*/
                            $scope.listCrash = res.data.data.data;
                        }
                    }
                });
            }else{
                return false;
            }
        };
    /*paging*/

    $scope.mergeDataCrash = function () {
        $http({
            url : $svc.bugServer+'report',
            method : 'get',
            paramSerializer : '$httpParamSerializerJQLike',
            params : $scope.report3
        }).then(function (res) {
            if(res.status===200){
                if(res.data.status===1){
                    /*paging*/
                        $scope.total_data=res.data.data.total;
                        $scope.current_page=res.data.data.current_page;
                        $scope.last_page=res.data.data.last_page;
                        $scope.total_page=$scope.last_page;
                        $scope.next_page_url=res.data.data.next_page_url;
                        $scope.prev_page_url=res.data.data.prev_page_url;
                        $scope.startNumber=res.data.data.from;
                        $scope.endNumber=res.data.data.to;
                        $scope.per_page=res.data.data.per_page;
                    /*paging*/
                    $scope.listCrash = res.data.data.data;
                }
            }
        });
    };

    /*====================Data Render====================*/
    //--------------------------Statistics----------------------------//

    //--------------------------Most----------------------------//
    $scope.dataView = {
        choose:'_view'
    };
    $scope.dataClick = {
        choose:'_click'
    };
    $scope.dataPlay = {
        choose:'_play'
    };


    $scope.viewFn = function () {
        $http({
            url : $svc.apiServer+"most",
            method : 'POST',
            headers : authHeader,
            data : $scope.dataView
        }).then(function(res){
            /*console.log(res);*/
            if(res.status === 200){
                //success
                if(res.data.status === 1){
                    $scope.list_view = res.data.data;
                    /*console.log($scope.list);*/
                }else{
                    if(res.data.status_code === 'ISNF'){
                        alert('Mohon maaf, Session anda telah habis');
                        window.location.href = $svc.baseURL+'log/out';
                    }else{
                        swal(
                            'Error : '+res.data.status_code,
                            '',
                            'error');
                    }
                }
            }else{
                alert(res.status);
            }
        });
    };
    $scope.clickFn = function () {
        $http({
            url : $svc.apiServer+"most",
            method : 'POST',
            headers : authHeader,
            data : $scope.dataClick
        }).then(function(res){
            /*console.log(res);*/
            if(res.status === 200){
                //success
                if(res.data.status === 1){
                    $scope.list_click = res.data.data;
                    /*console.log($scope.list);*/
                }else{
                    if(res.data.status_code === 'ISNF'){
                        alert('Mohon maaf, Session anda telah habis');
                        window.location.href = $svc.baseURL+'log/out';
                    }else{
                        swal(
                            'Error : '+res.data.status_code,
                            '',
                            'error');
                    }
                }
            }else{
                alert(res.status);
            }
        });
    };
    $scope.playFn = function () {
        $http({
            url : $svc.apiServer+"most",
            method : 'POST',
            headers : authHeader,
            data : $scope.dataPlay
        }).then(function(res){
            /*console.log(res);*/
            if(res.status === 200){
                //success
                if(res.data.status === 1){
                    $scope.list_play = res.data.data;
                    /*console.log($scope.list);*/
                }else{
                    if(res.data.status_code === 'ISNF'){
                        alert('Mohon maaf, Session anda telah habis');
                        window.location.href = $svc.baseURL+'log/out';
                    }else{
                        swal(
                            'Error : '+res.data.status_code,
                            '',
                            'error');
                    }
                }
            }else{
                alert(res.status);
            }
        });
    };

    $scope.$watch('viewTable.utama_id', function(newData,oldData) {
        /*console.log(newData);*/
        if(newData !== undefined){
            $scope.dataView.utama_id = newData;
            $scope.viewFn();
        }
    }, false);
    $scope.$watch('clickTable.utama_id', function(newData,oldData) {
        /*console.log(newData);*/
        if(newData !== undefined){
            $scope.dataClick.utama_id = newData;
            $scope.clickFn();
        }
    }, false);
    $scope.$watch('playTable.utama_id', function(newData,oldData) {
        /*console.log(newData);*/
        if(newData !== undefined){
            $scope.dataPlay.utama_id = newData;
            $scope.playFn();
        }
    }, false);

    //--------------------------/Most----------------------------//
}]);